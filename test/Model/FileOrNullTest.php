<?php
/**
 * FileOrNullTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Cms.
 *
 * Content management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\CmsClient;

use PHPUnit\Framework\TestCase;

/**
 * FileOrNullTest Class Doc Comment
 *
 * @category    Class
 * @description FileOrNull
 * @package     Ensi\CmsClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class FileOrNullTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "FileOrNull"
     */
    public function testFileOrNull()
    {
    }

    /**
     * Test attribute "path"
     */
    public function testPropertyPath()
    {
    }

    /**
     * Test attribute "root_path"
     */
    public function testPropertyRootPath()
    {
    }

    /**
     * Test attribute "url"
     */
    public function testPropertyUrl()
    {
    }
}
