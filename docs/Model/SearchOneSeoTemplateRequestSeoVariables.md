# # SearchOneSeoTemplateRequestSeoVariables

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fill** | **bool** | Заполнить seo-переменные в шаблоне. | [optional] 
**payload** | [**\Ensi\CmsClient\Dto\SearchOneSeoTemplateRequestSeoVariablesPayload**](SearchOneSeoTemplateRequestSeoVariablesPayload.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


