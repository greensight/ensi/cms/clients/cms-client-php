# # InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Image type (value from BannerImageTypeEnum) | [optional] 
**file** | [**\SplFileObject**](\SplFileObject.md) | Downloadable file | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


