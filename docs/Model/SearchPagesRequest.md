# # SearchPagesRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**filter** | [**object**](.md) |  | [optional] 
**pagination** | [**\Ensi\CmsClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


