# # MenuItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**url** | **string** | Ссылка | 
**name** | **string** | Название | 
**menu_id** | **int** | Идентификатор меню | 
**_lft** | **int** | Идентификатор элемента слева | 
**_rgt** | **int** | Идентификатор элемента справа | 
**parent_id** | **int** | Идентификатор родительского элемента | 
**active** | **bool** | Видимость пункта меню | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


