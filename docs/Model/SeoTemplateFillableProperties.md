# # SeoTemplateFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название шаблона | [optional] 
**type** | **int** | Тип шаблона из SeoTemplateTypeEnum | [optional] 
**header** | **string** | Заголовок h1 | [optional] 
**title** | **string** | Заголовок окна браузера | [optional] 
**description** | **string** | Описание страницы, мета-тег description | [optional] 
**seo_text** | **string** | SEO-текст. Выводится над футером в SEO-блоке. | [optional] 
**is_active** | **bool** | Активность шаблона | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


