# # CreateNameplateRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**code** | **string** |  | [optional] 
**background_color** | **string** |  | [optional] 
**text_color** | **string** |  | [optional] 
**is_active** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


