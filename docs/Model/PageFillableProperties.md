# # PageFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Наименование страницы | [optional] 
**slug** | **string** | Человекопонятный идентификатор для url | [optional] 
**content** | **string** | Html блок-контент | [optional] 
**is_active** | **bool** | Активность страницы | [optional] 
**active_from** | [**\DateTime**](\DateTime.md) | Дата и время начала публикации страницы | [optional] 
**active_to** | [**\DateTime**](\DateTime.md) | Дата и время окончания публикации страницы | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


