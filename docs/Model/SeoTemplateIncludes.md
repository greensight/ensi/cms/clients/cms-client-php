# # SeoTemplateIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\Ensi\CmsClient\Dto\SeoTemplateProduct[]**](SeoTemplateProduct.md) | Ссылка на продукт в PIM | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


