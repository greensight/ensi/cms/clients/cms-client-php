# # SearchOneMenuRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**filter** | [**object**](.md) |  | [optional] 
**include** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


