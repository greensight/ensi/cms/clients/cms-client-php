# # SeoTemplateReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания SEO-шаблона | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления SEO-шаблона | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


