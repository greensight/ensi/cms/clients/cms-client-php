# # SearchOneSeoTemplateRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter** | [**object**](.md) |  | [optional] 
**include** | **string[]** |  | [optional] 
**seo_variables** | [**\Ensi\CmsClient\Dto\SearchOneSeoTemplateRequestSeoVariables**](SearchOneSeoTemplateRequestSeoVariables.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


