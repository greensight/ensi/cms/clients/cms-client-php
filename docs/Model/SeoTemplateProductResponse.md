# # SeoTemplateProductResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\CmsClient\Dto\SeoTemplateProduct**](SeoTemplateProduct.md) |  | 
**meta** | [**object**](.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


