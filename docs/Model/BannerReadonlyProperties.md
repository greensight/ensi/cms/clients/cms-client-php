# # BannerReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**code** | **string** | Код баннера | 
**desktop_image** | [**\Ensi\CmsClient\Dto\FileOrNull**](FileOrNull.md) |  | 
**mobile_image** | [**\Ensi\CmsClient\Dto\FileOrNull**](FileOrNull.md) |  | 
**button_id** | **int** | Идентификатор кнопки | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


