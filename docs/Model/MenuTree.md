# # MenuTree

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название пункта | 
**url** | **string** | Ссылка | 
**active** | **bool** | Видимость пункта меню | 
**children** | **object[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


