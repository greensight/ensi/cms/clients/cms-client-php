# # BannerIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Ensi\CmsClient\Dto\BannerType**](BannerType.md) |  | [optional] 
**button** | [**\Ensi\CmsClient\Dto\BannerButton**](BannerButton.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


