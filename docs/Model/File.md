# # File

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **string** | Path to file relative to root of domain disk | [optional] 
**root_path** | **string** | Path to file relative to root of physical disk ensi | [optional] 
**url** | **string** | Link to download the file | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


