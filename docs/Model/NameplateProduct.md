# # NameplateProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**nameplate_id** | **int** |  | 
**product_id** | **int** | Product ID from PIM | 
**created_at** | [**\DateTime**](\DateTime.md) | Create time | 
**updated_at** | [**\DateTime**](\DateTime.md) | Update time | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


