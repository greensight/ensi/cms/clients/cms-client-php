# # Nameplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**created_at** | [**\DateTime**](\DateTime.md) | Create time | 
**updated_at** | [**\DateTime**](\DateTime.md) | Update time | 
**name** | **string** |  | [optional] 
**code** | **string** |  | [optional] 
**background_color** | **string** |  | [optional] 
**text_color** | **string** |  | [optional] 
**is_active** | **bool** |  | [optional] 
**product_links** | [**\Ensi\CmsClient\Dto\NameplateProduct[]**](NameplateProduct.md) | Link with product from PIM | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


