# # SearchSeoTemplateProductsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\CmsClient\Dto\SeoTemplateProduct[]**](SeoTemplateProduct.md) |  | 
**meta** | [**\Ensi\CmsClient\Dto\SearchPagesResponseMeta**](SearchPagesResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


