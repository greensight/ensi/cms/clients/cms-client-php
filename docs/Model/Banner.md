# # Banner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**code** | **string** | Код баннера | 
**desktop_image** | [**\Ensi\CmsClient\Dto\FileOrNull**](FileOrNull.md) |  | 
**mobile_image** | [**\Ensi\CmsClient\Dto\FileOrNull**](FileOrNull.md) |  | 
**button_id** | **int** | Идентификатор кнопки | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**name** | **string** | Название | [optional] 
**is_active** | **bool** | Активность | [optional] 
**sort** | **int** | Значение сортировки | [optional] 
**url** | **string** | Ссылка | [optional] 
**type_id** | **int** | Идентификатор типа | [optional] 
**button** | [**\Ensi\CmsClient\Dto\BannerButton**](BannerButton.md) |  | [optional] 
**type** | [**\Ensi\CmsClient\Dto\BannerType**](BannerType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


