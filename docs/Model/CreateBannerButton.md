# # CreateBannerButton

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **string** | Ссылка | [optional] 
**text** | **string** | Текст | [optional] 
**location** | **string** | Размещение (значение из BannerButtonLocationEnum) | [optional] 
**type** | **string** | Тип (значение из BannerButtonTypeEnum) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


