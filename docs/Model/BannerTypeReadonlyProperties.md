# # BannerTypeReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Identifier | 
**created_at** | [**\DateTime**](\DateTime.md) | Date of creation | 
**updated_at** | [**\DateTime**](\DateTime.md) | Date of update | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


