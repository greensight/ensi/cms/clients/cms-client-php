# # NameplateIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_links** | [**\Ensi\CmsClient\Dto\NameplateProduct[]**](NameplateProduct.md) | Link with product from PIM | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


