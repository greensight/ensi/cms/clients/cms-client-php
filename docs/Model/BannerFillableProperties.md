# # BannerFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название | [optional] 
**is_active** | **bool** | Активность | [optional] 
**sort** | **int** | Значение сортировки | [optional] 
**url** | **string** | Ссылка | [optional] 
**type_id** | **int** | Идентификатор типа | [optional] 
**button** | [**\Ensi\CmsClient\Dto\CreateBannerButton**](CreateBannerButton.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


