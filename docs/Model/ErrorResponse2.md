# # ErrorResponse2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**object**](.md) |  | 
**errors** | [**\Ensi\CmsClient\Dto\Error[]**](Error.md) | Array of errors | 
**meta** | [**object**](.md) | Object with meta information | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


