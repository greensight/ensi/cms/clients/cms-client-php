# Ensi\CmsClient\NameplatesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addNameplateProducts**](NameplatesApi.md#addNameplateProducts) | **POST** /nameplates/nameplates/{id}:add-products | Linking objects of Product to object of Nameplate
[**createNameplate**](NameplatesApi.md#createNameplate) | **POST** /nameplates/nameplates | Creating an object of Nameplate
[**deleteNameplate**](NameplatesApi.md#deleteNameplate) | **DELETE** /nameplates/nameplates/{id} | Deleting an object of Nameplate
[**deleteNameplateProducts**](NameplatesApi.md#deleteNameplateProducts) | **DELETE** /nameplates/nameplates/{id}:delete-products | Deleting links between objects of Product and object of Nameplate
[**getNameplate**](NameplatesApi.md#getNameplate) | **GET** /nameplates/nameplates/{id} | Get the object of Nameplate by ID
[**patchNameplate**](NameplatesApi.md#patchNameplate) | **PATCH** /nameplates/nameplates/{id} | Patching an object of Nameplate
[**searchNameplates**](NameplatesApi.md#searchNameplates) | **POST** /nameplates/nameplates:search | Search for objects of Nameplate



## addNameplateProducts

> \Ensi\CmsClient\Dto\EmptyDataResponse addNameplateProducts($id, $add_products_request)

Linking objects of Product to object of Nameplate

Linking objects of Product to object of Nameplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$add_products_request = new \Ensi\CmsClient\Dto\AddProductsRequest(); // \Ensi\CmsClient\Dto\AddProductsRequest | 

try {
    $result = $apiInstance->addNameplateProducts($id, $add_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->addNameplateProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **add_products_request** | [**\Ensi\CmsClient\Dto\AddProductsRequest**](../Model/AddProductsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createNameplate

> \Ensi\CmsClient\Dto\NameplateResponse createNameplate($create_nameplate_request)

Creating an object of Nameplate

Creating an object of Nameplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_nameplate_request = new \Ensi\CmsClient\Dto\CreateNameplateRequest(); // \Ensi\CmsClient\Dto\CreateNameplateRequest | 

try {
    $result = $apiInstance->createNameplate($create_nameplate_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->createNameplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_nameplate_request** | [**\Ensi\CmsClient\Dto\CreateNameplateRequest**](../Model/CreateNameplateRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\NameplateResponse**](../Model/NameplateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteNameplate

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteNameplate($id)

Deleting an object of Nameplate

Deleting an object of Nameplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID

try {
    $result = $apiInstance->deleteNameplate($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->deleteNameplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteNameplateProducts

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteNameplateProducts($id, $delete_products_request)

Deleting links between objects of Product and object of Nameplate

Deleting links between objects of Product and object of Nameplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$delete_products_request = new \Ensi\CmsClient\Dto\DeleteProductsRequest(); // \Ensi\CmsClient\Dto\DeleteProductsRequest | 

try {
    $result = $apiInstance->deleteNameplateProducts($id, $delete_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->deleteNameplateProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **delete_products_request** | [**\Ensi\CmsClient\Dto\DeleteProductsRequest**](../Model/DeleteProductsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getNameplate

> \Ensi\CmsClient\Dto\NameplateResponse getNameplate($id)

Get the object of Nameplate by ID

Get the object of Nameplate by ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID

try {
    $result = $apiInstance->getNameplate($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->getNameplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |

### Return type

[**\Ensi\CmsClient\Dto\NameplateResponse**](../Model/NameplateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchNameplate

> \Ensi\CmsClient\Dto\NameplateResponse patchNameplate($id, $patch_nameplate_request)

Patching an object of Nameplate

Patching an object of Nameplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$patch_nameplate_request = new \Ensi\CmsClient\Dto\PatchNameplateRequest(); // \Ensi\CmsClient\Dto\PatchNameplateRequest | 

try {
    $result = $apiInstance->patchNameplate($id, $patch_nameplate_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->patchNameplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **patch_nameplate_request** | [**\Ensi\CmsClient\Dto\PatchNameplateRequest**](../Model/PatchNameplateRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\NameplateResponse**](../Model/NameplateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchNameplates

> \Ensi\CmsClient\Dto\SearchNameplatesResponse searchNameplates($search_nameplates_request)

Search for objects of Nameplate

Search for objects of Nameplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_nameplates_request = new \Ensi\CmsClient\Dto\SearchNameplatesRequest(); // \Ensi\CmsClient\Dto\SearchNameplatesRequest | 

try {
    $result = $apiInstance->searchNameplates($search_nameplates_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->searchNameplates: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_nameplates_request** | [**\Ensi\CmsClient\Dto\SearchNameplatesRequest**](../Model/SearchNameplatesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchNameplatesResponse**](../Model/SearchNameplatesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

