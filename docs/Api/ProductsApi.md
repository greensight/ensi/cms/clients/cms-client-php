# Ensi\CmsClient\ProductsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addProductNameplates**](ProductsApi.md#addProductNameplates) | **POST** /products/products/{id}:add-nameplates | Linking objects of Nameplate to object of Product
[**deleteProductNameplates**](ProductsApi.md#deleteProductNameplates) | **DELETE** /products/products/{id}:delete-nameplates | Deleting links between objects of Nameplate and object of Product



## addProductNameplates

> \Ensi\CmsClient\Dto\EmptyDataResponse addProductNameplates($id, $add_nameplates_request)

Linking objects of Nameplate to object of Product

Linking objects of Nameplate to object of Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$add_nameplates_request = new \Ensi\CmsClient\Dto\AddNameplatesRequest(); // \Ensi\CmsClient\Dto\AddNameplatesRequest | 

try {
    $result = $apiInstance->addProductNameplates($id, $add_nameplates_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->addProductNameplates: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **add_nameplates_request** | [**\Ensi\CmsClient\Dto\AddNameplatesRequest**](../Model/AddNameplatesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductNameplates

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteProductNameplates($id, $delete_nameplates_request)

Deleting links between objects of Nameplate and object of Product

Deleting links between objects of Nameplate and object of Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$delete_nameplates_request = new \Ensi\CmsClient\Dto\DeleteNameplatesRequest(); // \Ensi\CmsClient\Dto\DeleteNameplatesRequest | 

try {
    $result = $apiInstance->deleteProductNameplates($id, $delete_nameplates_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->deleteProductNameplates: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **delete_nameplates_request** | [**\Ensi\CmsClient\Dto\DeleteNameplatesRequest**](../Model/DeleteNameplatesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

