# Ensi\CmsClient\SeoTemplateProductsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSeoTemplateProduct**](SeoTemplateProductsApi.md#getSeoTemplateProduct) | **GET** /seo/template-products/{id} | Get the object of SeoTemplateProduct
[**searchSeoTemplateProducts**](SeoTemplateProductsApi.md#searchSeoTemplateProducts) | **POST** /seo/template-products:search | Search for objects of SeoTemplateProduct



## getSeoTemplateProduct

> \Ensi\CmsClient\Dto\SeoTemplateProductResponse getSeoTemplateProduct($id)

Get the object of SeoTemplateProduct

Get the object of SeoTemplateProduct

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplateProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id

try {
    $result = $apiInstance->getSeoTemplateProduct($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplateProductsApi->getSeoTemplateProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |

### Return type

[**\Ensi\CmsClient\Dto\SeoTemplateProductResponse**](../Model/SeoTemplateProductResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchSeoTemplateProducts

> \Ensi\CmsClient\Dto\SearchSeoTemplateProductsResponse searchSeoTemplateProducts($search_seo_template_products_request)

Search for objects of SeoTemplateProduct

Search for objects of SeoTemplateProduct

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplateProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_seo_template_products_request = new \Ensi\CmsClient\Dto\SearchSeoTemplateProductsRequest(); // \Ensi\CmsClient\Dto\SearchSeoTemplateProductsRequest | 

try {
    $result = $apiInstance->searchSeoTemplateProducts($search_seo_template_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplateProductsApi->searchSeoTemplateProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_seo_template_products_request** | [**\Ensi\CmsClient\Dto\SearchSeoTemplateProductsRequest**](../Model/SearchSeoTemplateProductsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchSeoTemplateProductsResponse**](../Model/SearchSeoTemplateProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

