# Ensi\CmsClient\MenusApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchMenu**](MenusApi.md#searchMenu) | **POST** /contents/menus:search-one | Search for object of type Menu
[**searchMenus**](MenusApi.md#searchMenus) | **POST** /contents/menus:search | Search for objects of type Menu
[**updateMenuTrees**](MenusApi.md#updateMenuTrees) | **PUT** /contents/menus/{id}/trees | Updating the Menu tree



## searchMenu

> \Ensi\CmsClient\Dto\MenuResponse searchMenu($search_one_menu_request)

Search for object of type Menu

Search for object of type Menu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\MenusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_one_menu_request = new \Ensi\CmsClient\Dto\SearchOneMenuRequest(); // \Ensi\CmsClient\Dto\SearchOneMenuRequest | 

try {
    $result = $apiInstance->searchMenu($search_one_menu_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MenusApi->searchMenu: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_one_menu_request** | [**\Ensi\CmsClient\Dto\SearchOneMenuRequest**](../Model/SearchOneMenuRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\MenuResponse**](../Model/MenuResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchMenus

> \Ensi\CmsClient\Dto\SearchMenusResponse searchMenus($search_menus_request)

Search for objects of type Menu

Search for objects of type Menu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\MenusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_menus_request = new \Ensi\CmsClient\Dto\SearchMenusRequest(); // \Ensi\CmsClient\Dto\SearchMenusRequest | 

try {
    $result = $apiInstance->searchMenus($search_menus_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MenusApi->searchMenus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_menus_request** | [**\Ensi\CmsClient\Dto\SearchMenusRequest**](../Model/SearchMenusRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchMenusResponse**](../Model/SearchMenusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## updateMenuTrees

> \Ensi\CmsClient\Dto\UpdateMenuTreesResponse updateMenuTrees($id, $update_menu_trees_request)

Updating the Menu tree

Updating the Menu tree

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\MenusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$update_menu_trees_request = new \Ensi\CmsClient\Dto\UpdateMenuTreesRequest(); // \Ensi\CmsClient\Dto\UpdateMenuTreesRequest | 

try {
    $result = $apiInstance->updateMenuTrees($id, $update_menu_trees_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MenusApi->updateMenuTrees: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **update_menu_trees_request** | [**\Ensi\CmsClient\Dto\UpdateMenuTreesRequest**](../Model/UpdateMenuTreesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\UpdateMenuTreesResponse**](../Model/UpdateMenuTreesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

