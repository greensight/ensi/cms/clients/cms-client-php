# Ensi\CmsClient\NameplateProductsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getNameplateProduct**](NameplateProductsApi.md#getNameplateProduct) | **GET** /nameplates/nameplate-products/{id} | Get the object of ProductNameplateLink by ID
[**searchNameplateProducts**](NameplateProductsApi.md#searchNameplateProducts) | **POST** /nameplates/nameplate-products:search | Search for objects of ProductNameplateLink



## getNameplateProduct

> \Ensi\CmsClient\Dto\NameplateProductResponse getNameplateProduct($id)

Get the object of ProductNameplateLink by ID

Get the object of ProductNameplateLink by ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplateProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID

try {
    $result = $apiInstance->getNameplateProduct($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplateProductsApi->getNameplateProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |

### Return type

[**\Ensi\CmsClient\Dto\NameplateProductResponse**](../Model/NameplateProductResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchNameplateProducts

> \Ensi\CmsClient\Dto\SearchNameplateProductsResponse searchNameplateProducts($search_nameplate_products_request)

Search for objects of ProductNameplateLink

Search for objects of ProductNameplateLink

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\NameplateProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_nameplate_products_request = new \Ensi\CmsClient\Dto\SearchNameplateProductsRequest(); // \Ensi\CmsClient\Dto\SearchNameplateProductsRequest | 

try {
    $result = $apiInstance->searchNameplateProducts($search_nameplate_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplateProductsApi->searchNameplateProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_nameplate_products_request** | [**\Ensi\CmsClient\Dto\SearchNameplateProductsRequest**](../Model/SearchNameplateProductsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchNameplateProductsResponse**](../Model/SearchNameplateProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

