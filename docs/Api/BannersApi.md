# Ensi\CmsClient\BannersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBanner**](BannersApi.md#createBanner) | **POST** /contents/banners | Creating an object of type Banner
[**deleteBanner**](BannersApi.md#deleteBanner) | **DELETE** /contents/banners/{id} | Deleting an object of type Banner
[**deleteBannerFile**](BannersApi.md#deleteBannerFile) | **POST** /contents/banners/{id}:delete-file | Deleting a file for an object of type Banner
[**replaceBanner**](BannersApi.md#replaceBanner) | **PUT** /contents/banners/{id} | Replacing an object of type Banner
[**searchBanner**](BannersApi.md#searchBanner) | **POST** /contents/banners:search-one | Search for object of type Banner
[**searchBannerTypes**](BannersApi.md#searchBannerTypes) | **POST** /contents/banner-types:search | Search for objects of type BannerType
[**searchBanners**](BannersApi.md#searchBanners) | **POST** /contents/banners:search | Search for objects of type Banner
[**uploadBannerFile**](BannersApi.md#uploadBannerFile) | **POST** /contents/banners/{id}:upload-file | Uploading file for object type Banner



## createBanner

> \Ensi\CmsClient\Dto\BannerResponse createBanner($create_banner_request)

Creating an object of type Banner

Creating an object of type Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_banner_request = new \Ensi\CmsClient\Dto\CreateBannerRequest(); // \Ensi\CmsClient\Dto\CreateBannerRequest | 

try {
    $result = $apiInstance->createBanner($create_banner_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->createBanner: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_banner_request** | [**\Ensi\CmsClient\Dto\CreateBannerRequest**](../Model/CreateBannerRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\BannerResponse**](../Model/BannerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteBanner

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteBanner($id)

Deleting an object of type Banner

Deleting an object of type Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id

try {
    $result = $apiInstance->deleteBanner($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->deleteBanner: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteBannerFile

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteBannerFile($id, $delete_banner_file_request)

Deleting a file for an object of type Banner

Deleting a file for an object of type Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$delete_banner_file_request = new \Ensi\CmsClient\Dto\DeleteBannerFileRequest(); // \Ensi\CmsClient\Dto\DeleteBannerFileRequest | 

try {
    $result = $apiInstance->deleteBannerFile($id, $delete_banner_file_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->deleteBannerFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **delete_banner_file_request** | [**\Ensi\CmsClient\Dto\DeleteBannerFileRequest**](../Model/DeleteBannerFileRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceBanner

> \Ensi\CmsClient\Dto\BannerResponse replaceBanner($id, $replace_banner_request)

Replacing an object of type Banner

Replacing an object of type Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$replace_banner_request = new \Ensi\CmsClient\Dto\ReplaceBannerRequest(); // \Ensi\CmsClient\Dto\ReplaceBannerRequest | 

try {
    $result = $apiInstance->replaceBanner($id, $replace_banner_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->replaceBanner: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **replace_banner_request** | [**\Ensi\CmsClient\Dto\ReplaceBannerRequest**](../Model/ReplaceBannerRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\BannerResponse**](../Model/BannerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBanner

> \Ensi\CmsClient\Dto\BannerResponse searchBanner($search_one_banner_request)

Search for object of type Banner

Search for object of type Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_one_banner_request = new \Ensi\CmsClient\Dto\SearchOneBannerRequest(); // \Ensi\CmsClient\Dto\SearchOneBannerRequest | 

try {
    $result = $apiInstance->searchBanner($search_one_banner_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->searchBanner: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_one_banner_request** | [**\Ensi\CmsClient\Dto\SearchOneBannerRequest**](../Model/SearchOneBannerRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\BannerResponse**](../Model/BannerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBannerTypes

> \Ensi\CmsClient\Dto\SearchBannerTypesResponse searchBannerTypes($search_banner_types_request)

Search for objects of type BannerType

Search for objects of type BannerType

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_banner_types_request = new \Ensi\CmsClient\Dto\SearchBannerTypesRequest(); // \Ensi\CmsClient\Dto\SearchBannerTypesRequest | 

try {
    $result = $apiInstance->searchBannerTypes($search_banner_types_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->searchBannerTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_banner_types_request** | [**\Ensi\CmsClient\Dto\SearchBannerTypesRequest**](../Model/SearchBannerTypesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchBannerTypesResponse**](../Model/SearchBannerTypesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBanners

> \Ensi\CmsClient\Dto\SearchBannersResponse searchBanners($search_banners_request)

Search for objects of type Banner

Search for objects of type Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_banners_request = new \Ensi\CmsClient\Dto\SearchBannersRequest(); // \Ensi\CmsClient\Dto\SearchBannersRequest | 

try {
    $result = $apiInstance->searchBanners($search_banners_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->searchBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_banners_request** | [**\Ensi\CmsClient\Dto\SearchBannersRequest**](../Model/SearchBannersRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchBannersResponse**](../Model/SearchBannersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadBannerFile

> \Ensi\CmsClient\Dto\BannerFileResponse uploadBannerFile($id, $type, $file)

Uploading file for object type Banner

Uploading file for object type Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$type = 'type_example'; // string | Image type (value from BannerImageTypeEnum)
$file = "/path/to/file.txt"; // \SplFileObject | Downloadable file

try {
    $result = $apiInstance->uploadBannerFile($id, $type, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->uploadBannerFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **type** | **string**| Image type (value from BannerImageTypeEnum) | [optional]
 **file** | **\SplFileObject****\SplFileObject**| Downloadable file | [optional]

### Return type

[**\Ensi\CmsClient\Dto\BannerFileResponse**](../Model/BannerFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

