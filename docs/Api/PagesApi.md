# Ensi\CmsClient\PagesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPage**](PagesApi.md#createPage) | **POST** /contents/pages | Creating an object of type Page
[**deletePage**](PagesApi.md#deletePage) | **DELETE** /contents/pages/{id} | Deleting an object of type Page
[**getPage**](PagesApi.md#getPage) | **GET** /contents/pages/{id} | Getting an object of type Page
[**patchPage**](PagesApi.md#patchPage) | **PATCH** /contents/pages/{id} | Patching objects of type Page
[**searchPage**](PagesApi.md#searchPage) | **POST** /contents/pages:search-one | Search for object of type Page
[**searchPages**](PagesApi.md#searchPages) | **POST** /contents/pages:search | Search for objects of type Page



## createPage

> \Ensi\CmsClient\Dto\PageResponse createPage($create_page_request)

Creating an object of type Page

Creating an object of type Page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\PagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_page_request = new \Ensi\CmsClient\Dto\CreatePageRequest(); // \Ensi\CmsClient\Dto\CreatePageRequest | 

try {
    $result = $apiInstance->createPage($create_page_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PagesApi->createPage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_page_request** | [**\Ensi\CmsClient\Dto\CreatePageRequest**](../Model/CreatePageRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\PageResponse**](../Model/PageResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePage

> \Ensi\CmsClient\Dto\EmptyDataResponse deletePage($id)

Deleting an object of type Page

Deleting an object of type Page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\PagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id

try {
    $result = $apiInstance->deletePage($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PagesApi->deletePage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPage

> \Ensi\CmsClient\Dto\PageResponse getPage($id)

Getting an object of type Page

Getting an object of type Page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\PagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id

try {
    $result = $apiInstance->getPage($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PagesApi->getPage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |

### Return type

[**\Ensi\CmsClient\Dto\PageResponse**](../Model/PageResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPage

> \Ensi\CmsClient\Dto\PageResponse patchPage($id, $patch_page_request)

Patching objects of type Page

Patching objects of type Page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\PagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$patch_page_request = new \Ensi\CmsClient\Dto\PatchPageRequest(); // \Ensi\CmsClient\Dto\PatchPageRequest | 

try {
    $result = $apiInstance->patchPage($id, $patch_page_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PagesApi->patchPage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **patch_page_request** | [**\Ensi\CmsClient\Dto\PatchPageRequest**](../Model/PatchPageRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\PageResponse**](../Model/PageResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPage

> \Ensi\CmsClient\Dto\PageResponse searchPage($search_one_page_request)

Search for object of type Page

Search for object of type Page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\PagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_one_page_request = new \Ensi\CmsClient\Dto\SearchOnePageRequest(); // \Ensi\CmsClient\Dto\SearchOnePageRequest | 

try {
    $result = $apiInstance->searchPage($search_one_page_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PagesApi->searchPage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_one_page_request** | [**\Ensi\CmsClient\Dto\SearchOnePageRequest**](../Model/SearchOnePageRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\PageResponse**](../Model/PageResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPages

> \Ensi\CmsClient\Dto\SearchPagesResponse searchPages($search_pages_request)

Search for objects of type Page

Search for objects of type Page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\PagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_pages_request = new \Ensi\CmsClient\Dto\SearchPagesRequest(); // \Ensi\CmsClient\Dto\SearchPagesRequest | 

try {
    $result = $apiInstance->searchPages($search_pages_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PagesApi->searchPages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_pages_request** | [**\Ensi\CmsClient\Dto\SearchPagesRequest**](../Model/SearchPagesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchPagesResponse**](../Model/SearchPagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

