# Ensi\CmsClient\SeoTemplatesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addSeoTemplateProducts**](SeoTemplatesApi.md#addSeoTemplateProducts) | **POST** /seo/templates/{id}:add-products | Linking objects of Product to object of SeoTemplate
[**createSeoTemplate**](SeoTemplatesApi.md#createSeoTemplate) | **POST** /seo/templates | Creating an object of SeoTemplate
[**deleteSeoTemplate**](SeoTemplatesApi.md#deleteSeoTemplate) | **DELETE** /seo/templates/{id} | Deleting an object of SeoTemplate
[**deleteSeoTemplateProducts**](SeoTemplatesApi.md#deleteSeoTemplateProducts) | **DELETE** /seo/templates/{id}:delete-products | Deleting links between objects of Product and object of SeoTemplate
[**getSeoTemplate**](SeoTemplatesApi.md#getSeoTemplate) | **GET** /seo/templates/{id} | Get the object of SeoTemplate by ID
[**patchSeoTemplate**](SeoTemplatesApi.md#patchSeoTemplate) | **PATCH** /seo/templates/{id} | Patching an object of  SeoTemplate
[**searchOneSeoTemplate**](SeoTemplatesApi.md#searchOneSeoTemplate) | **POST** /seo/templates:search-one | Search for an object of SeoTemplate
[**searchSeoTemplates**](SeoTemplatesApi.md#searchSeoTemplates) | **POST** /seo/templates:search | Search for objects of SeoTemplate



## addSeoTemplateProducts

> \Ensi\CmsClient\Dto\EmptyDataResponse addSeoTemplateProducts($id, $add_seo_template_products_request)

Linking objects of Product to object of SeoTemplate

Linking objects of Product to object of SeoTemplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$add_seo_template_products_request = new \Ensi\CmsClient\Dto\AddSeoTemplateProductsRequest(); // \Ensi\CmsClient\Dto\AddSeoTemplateProductsRequest | 

try {
    $result = $apiInstance->addSeoTemplateProducts($id, $add_seo_template_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplatesApi->addSeoTemplateProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **add_seo_template_products_request** | [**\Ensi\CmsClient\Dto\AddSeoTemplateProductsRequest**](../Model/AddSeoTemplateProductsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createSeoTemplate

> \Ensi\CmsClient\Dto\SeoTemplateResponse createSeoTemplate($seo_template_for_create)

Creating an object of SeoTemplate

Creating an object of SeoTemplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seo_template_for_create = new \Ensi\CmsClient\Dto\SeoTemplateForCreate(); // \Ensi\CmsClient\Dto\SeoTemplateForCreate | 

try {
    $result = $apiInstance->createSeoTemplate($seo_template_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplatesApi->createSeoTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seo_template_for_create** | [**\Ensi\CmsClient\Dto\SeoTemplateForCreate**](../Model/SeoTemplateForCreate.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SeoTemplateResponse**](../Model/SeoTemplateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteSeoTemplate

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteSeoTemplate($id)

Deleting an object of SeoTemplate

Deleting an object of SeoTemplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id

try {
    $result = $apiInstance->deleteSeoTemplate($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplatesApi->deleteSeoTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteSeoTemplateProducts

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteSeoTemplateProducts($id, $delete_seo_template_products_request)

Deleting links between objects of Product and object of SeoTemplate

Deleting links between objects of Product and object of SeoTemplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$delete_seo_template_products_request = new \Ensi\CmsClient\Dto\DeleteSeoTemplateProductsRequest(); // \Ensi\CmsClient\Dto\DeleteSeoTemplateProductsRequest | 

try {
    $result = $apiInstance->deleteSeoTemplateProducts($id, $delete_seo_template_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplatesApi->deleteSeoTemplateProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **delete_seo_template_products_request** | [**\Ensi\CmsClient\Dto\DeleteSeoTemplateProductsRequest**](../Model/DeleteSeoTemplateProductsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getSeoTemplate

> \Ensi\CmsClient\Dto\SeoTemplateResponse getSeoTemplate($id)

Get the object of SeoTemplate by ID

Get the object of SeoTemplate by ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id

try {
    $result = $apiInstance->getSeoTemplate($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplatesApi->getSeoTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |

### Return type

[**\Ensi\CmsClient\Dto\SeoTemplateResponse**](../Model/SeoTemplateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchSeoTemplate

> \Ensi\CmsClient\Dto\SeoTemplateResponse patchSeoTemplate($id, $seo_template_for_patch)

Patching an object of  SeoTemplate

Patching an object of  SeoTemplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$seo_template_for_patch = new \Ensi\CmsClient\Dto\SeoTemplateForPatch(); // \Ensi\CmsClient\Dto\SeoTemplateForPatch | 

try {
    $result = $apiInstance->patchSeoTemplate($id, $seo_template_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplatesApi->patchSeoTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **seo_template_for_patch** | [**\Ensi\CmsClient\Dto\SeoTemplateForPatch**](../Model/SeoTemplateForPatch.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SeoTemplateResponse**](../Model/SeoTemplateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneSeoTemplate

> \Ensi\CmsClient\Dto\SeoTemplateResponse searchOneSeoTemplate($search_one_seo_template_request)

Search for an object of SeoTemplate

Search for an object of SeoTemplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_one_seo_template_request = new \Ensi\CmsClient\Dto\SearchOneSeoTemplateRequest(); // \Ensi\CmsClient\Dto\SearchOneSeoTemplateRequest | 

try {
    $result = $apiInstance->searchOneSeoTemplate($search_one_seo_template_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplatesApi->searchOneSeoTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_one_seo_template_request** | [**\Ensi\CmsClient\Dto\SearchOneSeoTemplateRequest**](../Model/SearchOneSeoTemplateRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SeoTemplateResponse**](../Model/SeoTemplateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchSeoTemplates

> \Ensi\CmsClient\Dto\SearchSeoTemplatesResponse searchSeoTemplates($search_seo_templates_request)

Search for objects of SeoTemplate

Search for objects of SeoTemplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\SeoTemplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_seo_templates_request = new \Ensi\CmsClient\Dto\SearchSeoTemplatesRequest(); // \Ensi\CmsClient\Dto\SearchSeoTemplatesRequest | 

try {
    $result = $apiInstance->searchSeoTemplates($search_seo_templates_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeoTemplatesApi->searchSeoTemplates: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_seo_templates_request** | [**\Ensi\CmsClient\Dto\SearchSeoTemplatesRequest**](../Model/SearchSeoTemplatesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchSeoTemplatesResponse**](../Model/SearchSeoTemplatesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

