<?php
/**
 * BannerTypeEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Cms.
 *
 * Content management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\CmsClient\Dto;
use \Ensi\CmsClient\ObjectSerializer;

/**
 * BannerTypeEnum Class Doc Comment
 *
 * @category Class
 * @description Banner type:   * &#x60;main&#x60; - Main banner
 * @package  Ensi\CmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class BannerTypeEnum
{
    
    /** Главный баннер */
    public const MAIN_CODE = 'main';
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::MAIN_CODE,
        ];
    }

    /**
    * Gets allowable values and titles of the enum
    * @return string[]
    */
    public static function getDescriptions(): array
    {
        return [
            self::MAIN_CODE => 'Главный баннер',
        ];
    }
}


