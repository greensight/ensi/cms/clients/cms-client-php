<?php
/**
 * BannerButtonLocationEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Cms.
 *
 * Content management
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\CmsClient\Dto;
use \Ensi\CmsClient\ObjectSerializer;

/**
 * BannerButtonLocationEnum Class Doc Comment
 *
 * @category Class
 * @description Расположение кнопки:   * &#x60;top_left&#x60; - Сверху слева   * &#x60;top&#x60; - Сверху   * &#x60;top_right&#x60; - Сверху справа   * &#x60;right&#x60; - Справа   * &#x60;bottom_right&#x60; - Снизу справа   * &#x60;bottom&#x60; - Снизу   * &#x60;bottom_left&#x60; - Снизу слева   * &#x60;left&#x60; - Слева
 * @package  Ensi\CmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class BannerButtonLocationEnum
{
    
    /** Сверху слева */
    public const LOCATION_TOP_LEFT = 'top_left';
    
    /** Сверху */
    public const LOCATION_TOP = 'top';
    
    /** Сверху справа */
    public const LOCATION_TOP_RIGHT = 'top_right';
    
    /** Справа */
    public const LOCATION_RIGHT = 'right';
    
    /** Снизу справа */
    public const LOCATION_BOTTOM_RIGHT = 'bottom_right';
    
    /** Снизу */
    public const LOCATION_BOTTOM = 'bottom';
    
    /** Снизу слева */
    public const LOCATION_BOTTOM_LEFT = 'bottom_left';
    
    /** Слева */
    public const LOCATION_LEFT = 'left';
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::LOCATION_TOP_LEFT,
            self::LOCATION_TOP,
            self::LOCATION_TOP_RIGHT,
            self::LOCATION_RIGHT,
            self::LOCATION_BOTTOM_RIGHT,
            self::LOCATION_BOTTOM,
            self::LOCATION_BOTTOM_LEFT,
            self::LOCATION_LEFT,
        ];
    }

    /**
    * Gets allowable values and titles of the enum
    * @return string[]
    */
    public static function getDescriptions(): array
    {
        return [
            self::LOCATION_TOP_LEFT => 'Сверху слева',
            self::LOCATION_TOP => 'Сверху',
            self::LOCATION_TOP_RIGHT => 'Сверху справа',
            self::LOCATION_RIGHT => 'Справа',
            self::LOCATION_BOTTOM_RIGHT => 'Снизу справа',
            self::LOCATION_BOTTOM => 'Снизу',
            self::LOCATION_BOTTOM_LEFT => 'Снизу слева',
            self::LOCATION_LEFT => 'Слева',
        ];
    }
}


